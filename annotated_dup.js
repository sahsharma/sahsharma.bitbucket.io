var annotated_dup =
[
    [ "BLE", "namespace_b_l_e.html", "namespace_b_l_e" ],
    [ "BT_UI", "namespace_b_t___u_i.html", "namespace_b_t___u_i" ],
    [ "ClosedLoop", "namespace_closed_loop.html", "namespace_closed_loop" ],
    [ "DRV8847_MotorDriver", "namespace_d_r_v8847___motor_driver.html", "namespace_d_r_v8847___motor_driver" ],
    [ "Encoder", "namespace_encoder.html", "namespace_encoder" ],
    [ "FSM_Elevator", "namespace_f_s_m___elevator.html", "namespace_f_s_m___elevator" ],
    [ "ME305_Lab2", "namespace_m_e305___lab2.html", "namespace_m_e305___lab2" ],
    [ "UI", null, [
      [ "UI_Encoder", "class_u_i_1_1_u_i___encoder.html", "class_u_i_1_1_u_i___encoder" ]
    ] ],
    [ "UI_detaGen", "namespace_u_i__deta_gen.html", "namespace_u_i__deta_gen" ],
    [ "UI_detaGenRefTracker", "namespace_u_i__deta_gen_ref_tracker.html", "namespace_u_i__deta_gen_ref_tracker" ],
    [ "UI_detaGenStepResponse", "namespace_u_i__deta_gen_step_response.html", "namespace_u_i__deta_gen_step_response" ],
    [ "UI_frontend", "namespace_u_i__frontend.html", "namespace_u_i__frontend" ],
    [ "UI_frontendRefTracker", "namespace_u_i__frontend_ref_tracker.html", "namespace_u_i__frontend_ref_tracker" ],
    [ "UI_frontendStepResponse", "namespace_u_i__frontend_step_response.html", "namespace_u_i__frontend_step_response" ]
];