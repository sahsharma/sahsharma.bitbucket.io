var main__lab3_8py =
[
    [ "Bus", "main__lab3_8py.html#a8803662ef1011d699bccb88f1388f253", null ],
    [ "ChannelA", "main__lab3_8py.html#a749bf9c4ebcc66d19d8687debdae681e", null ],
    [ "ChannelB", "main__lab3_8py.html#ad09d59756dd4c2bd2f61380d68df0a36", null ],
    [ "CPR", "main__lab3_8py.html#a4f294f64a4f48c80f3b5cbe24a7d5e91", null ],
    [ "Interval1", "main__lab3_8py.html#a27f97acc426f4032776c20f7042e5362", null ],
    [ "Interval2", "main__lab3_8py.html#ad1cd5eaa589aa56e0baaf9152c4c46da", null ],
    [ "Period", "main__lab3_8py.html#a90aabf75651060845a9b1a05d124e264", null ],
    [ "PinA", "main__lab3_8py.html#aec3fbad37115366e23d8fe88bdf3809c", null ],
    [ "PinB", "main__lab3_8py.html#af1985b3083c31cdbe582cc15fb9ffac5", null ],
    [ "Prescaler", "main__lab3_8py.html#abd9637c84a2944bebe27c9a8a70cde92", null ],
    [ "Reduction", "main__lab3_8py.html#a79cac23cf7a41b57c3ae938698d32726", null ],
    [ "task1", "main__lab3_8py.html#abcdc3952d2b8bb17d70c168a25218d22", null ],
    [ "task2", "main__lab3_8py.html#ac40651557ae92fdaea94cc29edd2e57b", null ],
    [ "Timer", "main__lab3_8py.html#a692ded726b92d83e17ed87eb0a08c946", null ]
];