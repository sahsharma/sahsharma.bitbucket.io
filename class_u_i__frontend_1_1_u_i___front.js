var class_u_i__frontend_1_1_u_i___front =
[
    [ "__init__", "class_u_i__frontend_1_1_u_i___front.html#af1686825151b0052e428be9efc2c6497", null ],
    [ "run", "class_u_i__frontend_1_1_u_i___front.html#a76ee339c65c350c9777cd98efd591c5b", null ],
    [ "sendG", "class_u_i__frontend_1_1_u_i___front.html#a32b7276850123b9a0de9940e27688cb7", null ],
    [ "sendS", "class_u_i__frontend_1_1_u_i___front.html#a4862b44301ac8d661f35b36b105ea32a", null ],
    [ "transitionTo", "class_u_i__frontend_1_1_u_i___front.html#a5a1c7a3e579d664e2a8d3034c9e7ce0e", null ],
    [ "arrayForCSV", "class_u_i__frontend_1_1_u_i___front.html#a585239be0904f0ead89c933b19fa7873", null ],
    [ "curr_time", "class_u_i__frontend_1_1_u_i___front.html#af68d30216e32f6d47234dc88ce9094c0", null ],
    [ "interval", "class_u_i__frontend_1_1_u_i___front.html#a378ee1f8e020b50221e5a66d1a2bf756", null ],
    [ "lineFromBoard", "class_u_i__frontend_1_1_u_i___front.html#afe26f4c6d234538afa41aa4f0b6931a4", null ],
    [ "next_time", "class_u_i__frontend_1_1_u_i___front.html#aa4d552b6c0baf05df67fad125d01bef7", null ],
    [ "posArray", "class_u_i__frontend_1_1_u_i___front.html#a01ca5a31d0c5e8b20cf7fee2536e73ab", null ],
    [ "runs", "class_u_i__frontend_1_1_u_i___front.html#a0ba1a37a7d55513d119bbba2102b4afc", null ],
    [ "ser", "class_u_i__frontend_1_1_u_i___front.html#a4e8b111e4dada756c9b7b5f6f9355799", null ],
    [ "splitLine", "class_u_i__frontend_1_1_u_i___front.html#a6e3ad8dd35cab124ebd60363cfd8b8d7", null ],
    [ "start_time", "class_u_i__frontend_1_1_u_i___front.html#a71eb7e0d51e4ffd0a1e79eda9fb1b281", null ],
    [ "state", "class_u_i__frontend_1_1_u_i___front.html#ad98ad30d3339cbc7e1994ffa8a5096fd", null ],
    [ "timeArray", "class_u_i__frontend_1_1_u_i___front.html#a7de95c075ec11e7e72ba39d3cec191a7", null ]
];