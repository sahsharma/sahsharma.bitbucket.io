var class_closed_loop_1_1_c_l =
[
    [ "__init__", "class_closed_loop_1_1_c_l.html#a6b0c9961522d5c39705690e9f7192f20", null ],
    [ "disable", "class_closed_loop_1_1_c_l.html#ab27f6788f8a4c954726d7dd6241694d9", null ],
    [ "getKp", "class_closed_loop_1_1_c_l.html#a0da9a5a678de29294717f5b44be0a1ef", null ],
    [ "getLevel", "class_closed_loop_1_1_c_l.html#a7b2993e1208e9c8808a4a4525b132c5f", null ],
    [ "getOmegaRef", "class_closed_loop_1_1_c_l.html#a3d72fb9f84f0f7d217f5c29df1a9bc55", null ],
    [ "run", "class_closed_loop_1_1_c_l.html#af63b4f6916c933c8937ec417f3c3a533", null ],
    [ "setKp", "class_closed_loop_1_1_c_l.html#a81bcbe547076196bed32f9b260107066", null ],
    [ "setOmega", "class_closed_loop_1_1_c_l.html#a1b2e792371872dfe7167a5f889d0feca", null ],
    [ "setOmegaRef", "class_closed_loop_1_1_c_l.html#a076bbc7c11926ee9d4211f6b5558f231", null ],
    [ "curr_time", "class_closed_loop_1_1_c_l.html#a93f1cd5dfd501f1b524456f1f9c27983", null ],
    [ "dellevel", "class_closed_loop_1_1_c_l.html#a02647eeb63b9d7794a27d4b0f8a13904", null ],
    [ "interval", "class_closed_loop_1_1_c_l.html#aeb28c8eb44b48d4189002ad7547aa51f", null ],
    [ "Kp", "class_closed_loop_1_1_c_l.html#a07f9dd6cc627a818d5f56a8611e4f496", null ],
    [ "next_time", "class_closed_loop_1_1_c_l.html#a7385a6a10fde7f4b6ff5ec9183e514ef", null ],
    [ "omega", "class_closed_loop_1_1_c_l.html#a2e26a4a0351a5c26de912d1b3889f85b", null ],
    [ "omegaRef", "class_closed_loop_1_1_c_l.html#a53113d8205a6c40840e07fcc84da564a", null ],
    [ "runs", "class_closed_loop_1_1_c_l.html#a58b77b06a37177dc4b97020aa12aeea8", null ],
    [ "start_time", "class_closed_loop_1_1_c_l.html#a864c581377bd2b872992ca8827cf1dbd", null ]
];