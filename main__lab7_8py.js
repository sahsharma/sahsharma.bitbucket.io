var main__lab7_8py =
[
    [ "ChannelA", "main__lab7_8py.html#a2146c547a3c1056d51eb8337947cdcf1", null ],
    [ "ChannelB", "main__lab7_8py.html#a51908f643ef9df9401d7e1941bc76d3c", null ],
    [ "CPR", "main__lab7_8py.html#ab5178673b26aca762fc48e687efb905e", null ],
    [ "datapoints", "main__lab7_8py.html#a5a8dbbe0e0d571bb29db94d6b60d2b16", null ],
    [ "Interval1", "main__lab7_8py.html#ac80fc8fed6f8bd0dc359e0a0a86f9edb", null ],
    [ "Interval2", "main__lab7_8py.html#ac0ea5408b7fed39796049aa57a413e81", null ],
    [ "Interval3", "main__lab7_8py.html#a116eebd3365ccbc8ffa6a4df32abaefd", null ],
    [ "MD", "main__lab7_8py.html#a2bd8a1382f2bcc64792eb801e054ccf3", null ],
    [ "myuart", "main__lab7_8py.html#ab2d4fe4eae80add90327fe2e9cd1d659", null ],
    [ "Period", "main__lab7_8py.html#a24920c0b5b24a95324c06781eaed127d", null ],
    [ "PinA", "main__lab7_8py.html#a535bb4ae446a48f9f37373b330d9a84a", null ],
    [ "PinB", "main__lab7_8py.html#a674127a1c39ba512d138f040a016e559", null ],
    [ "Prescaler", "main__lab7_8py.html#a4e406c415e4b9cc393de32802312aa38", null ],
    [ "Reduction", "main__lab7_8py.html#a62a22ef79b734df8dd948c2db3be0e33", null ],
    [ "sampleRate", "main__lab7_8py.html#a8df324a2a3bbe75afa148a9236f1112d", null ],
    [ "sampleTime", "main__lab7_8py.html#ac282412d8d511c5e97b2cae025d730fa", null ],
    [ "task1", "main__lab7_8py.html#a773e3c146c5b1e31e7e1545a596441b6", null ],
    [ "task2", "main__lab7_8py.html#a32f490e23b7e0802115472c5b88d4002", null ],
    [ "task3", "main__lab7_8py.html#ae5c04500fd4990d8384b8f0873050e97", null ],
    [ "Timer", "main__lab7_8py.html#af32297be4c29bc2f90d0f09c0af7b0ea", null ]
];