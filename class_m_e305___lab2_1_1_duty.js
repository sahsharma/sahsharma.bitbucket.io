var class_m_e305___lab2_1_1_duty =
[
    [ "__init__", "class_m_e305___lab2_1_1_duty.html#abfcfc1dd3c9f25789603b6a9225c9240", null ],
    [ "Saw", "class_m_e305___lab2_1_1_duty.html#afed9c098d783cd0c3644188cc70e3b07", null ],
    [ "Sine", "class_m_e305___lab2_1_1_duty.html#a34b7dd3a65eb7f1fae44483838dbe7e2", null ],
    [ "amp", "class_m_e305___lab2_1_1_duty.html#a3ed790a496c927a8952472edd7fde509", null ],
    [ "offset", "class_m_e305___lab2_1_1_duty.html#ac85c5b23e46057e9b5bda2a0f4c5036d", null ],
    [ "period", "class_m_e305___lab2_1_1_duty.html#a8d7c55bf5c20d1c7e3d60d8f81d49b84", null ],
    [ "phase", "class_m_e305___lab2_1_1_duty.html#a185bb6505e7155f9b63616d730da7d9a", null ],
    [ "time", "class_m_e305___lab2_1_1_duty.html#acd57623ed40065f4b67e7be3826ab292", null ]
];