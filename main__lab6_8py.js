var main__lab6_8py =
[
    [ "ChannelA", "main__lab6_8py.html#a7e106629d3afe1c68e2497507c291611", null ],
    [ "ChannelB", "main__lab6_8py.html#a9796b2c1a5d67ccf60c0b8b9fcebb9c0", null ],
    [ "CPR", "main__lab6_8py.html#af0c86822b88ffa994a7973723ce28dfc", null ],
    [ "datapoints", "main__lab6_8py.html#a24abd52a1ce3249005fa9dab5494fa6b", null ],
    [ "Interval1", "main__lab6_8py.html#acffbc393c34b04435d9795b6b54bc070", null ],
    [ "Interval2", "main__lab6_8py.html#aac723f5c55e8528889fd3c4254dcb225", null ],
    [ "Interval3", "main__lab6_8py.html#a5ef920ef2c564df29ed071c73e0b81fb", null ],
    [ "MD", "main__lab6_8py.html#a2da05164cb29cd18bf709b97cdfd8d54", null ],
    [ "myuart", "main__lab6_8py.html#a6e5991b92f67fe11f5faea7c5a6c0f8f", null ],
    [ "Period", "main__lab6_8py.html#a248e5861673b5a62ae8a40c41bfdd301", null ],
    [ "PinA", "main__lab6_8py.html#a8125db2e8556ea2aa8d81e044dc5ab89", null ],
    [ "PinB", "main__lab6_8py.html#a82cdc02d7a14f1e4b1979ca57bbfeca5", null ],
    [ "Prescaler", "main__lab6_8py.html#a1096660c35e9b10fc40fef9f42760c9b", null ],
    [ "Reduction", "main__lab6_8py.html#a9a0acc78227a9564a979e3b61bef4afc", null ],
    [ "sampleRate", "main__lab6_8py.html#ab22ce69bb88e37d9eb4a4b5ba237c8eb", null ],
    [ "sampleTime", "main__lab6_8py.html#a5f2e9133e73907e5693badcd199af865", null ],
    [ "task1", "main__lab6_8py.html#a88bcea82c31cf910298a96c6428275f9", null ],
    [ "task2", "main__lab6_8py.html#ab6ed3ca961a1b992cee1e6e60bf7a658", null ],
    [ "task3", "main__lab6_8py.html#a47b664d9b64025d83b2c5fbb6f882c5a", null ],
    [ "Timer", "main__lab6_8py.html#a16239c4f06143c9e16bb3949af5ba085", null ]
];