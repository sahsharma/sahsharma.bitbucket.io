var main__lab4_8py =
[
    [ "ChannelA", "main__lab4_8py.html#a73f8e681395f8f087ccddcefcc78e2d6", null ],
    [ "ChannelB", "main__lab4_8py.html#ad871e66bf4c600cc6b17da743c41d2e1", null ],
    [ "CPR", "main__lab4_8py.html#a9e4459384030e72ece67d998ad37ac8b", null ],
    [ "DataPoints", "main__lab4_8py.html#a8872fdbe75becefa5f03434f755e6c29", null ],
    [ "Interval1", "main__lab4_8py.html#a6cfd6dd25c642c506e9bcc719433d992", null ],
    [ "Interval2", "main__lab4_8py.html#a6cc0b7799a80f664693ff667ca231a82", null ],
    [ "MyUart", "main__lab4_8py.html#a001601889e5025d21617fad3d1bf0fa5", null ],
    [ "Period", "main__lab4_8py.html#a97eacc710d64f8f5ac74e3acb7088cd2", null ],
    [ "PinA", "main__lab4_8py.html#a9e3a2a4582ccc0046e064e6cfa840aa4", null ],
    [ "PinB", "main__lab4_8py.html#af47fd9c2f3a134db17994ef5bb78f5a7", null ],
    [ "Prescaler", "main__lab4_8py.html#a9b25763a578ac560d6ade9d80faa0eb2", null ],
    [ "Reduction", "main__lab4_8py.html#a2fef0c2a207d6c1016e450780b8ed18e", null ],
    [ "SampleRate", "main__lab4_8py.html#a9ee4733e6630460b9f9fdf58b8f541e8", null ],
    [ "task1", "main__lab4_8py.html#ad05c5526a786c121cf793f90bccb946b", null ],
    [ "task2", "main__lab4_8py.html#ada547cce4a4cec82bbb55895918dbb66", null ],
    [ "Timer", "main__lab4_8py.html#a85f6d5810697142b29a7e1935f4aa825", null ]
];