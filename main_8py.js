var main_8py =
[
    [ "ChannelA", "main_8py.html#a4aff9156ac9c96b74ef6a2af24adb28f", null ],
    [ "ChannelB", "main_8py.html#ad08d09741eb7b4da76982b80a2584e64", null ],
    [ "CPR", "main_8py.html#a0e2444e8f54f7120687d86073165cccf", null ],
    [ "datapoints", "main_8py.html#a4e7d3954608e1d35f23f137d0fa8fabf", null ],
    [ "Interval1", "main_8py.html#a8e68f633350942b70a834f37a6e6444a", null ],
    [ "Interval2", "main_8py.html#a795390311d633b0fa0b318aa0d67f4c6", null ],
    [ "Interval3", "main_8py.html#ae56de55bc5b5fffccdc95761b742efae", null ],
    [ "MD", "main_8py.html#a080e4abe227e7810f50e9f211b060a7f", null ],
    [ "myuart", "main_8py.html#ac42c49539d72258115e8f934b5d52311", null ],
    [ "Period", "main_8py.html#a11642cbe1637f0eb788c42f9f59cd49d", null ],
    [ "PinA", "main_8py.html#a9f570842a9ba656b1302b072af617286", null ],
    [ "PinB", "main_8py.html#a95433d17d6c595aa095f9cd048b0606d", null ],
    [ "Prescaler", "main_8py.html#a0a11292c1d62d916f6216d68fbc33284", null ],
    [ "Reduction", "main_8py.html#a991ba375be4fb63e1cb1602c706bfa98", null ],
    [ "sampleRate", "main_8py.html#aaf0e0b994c6e24a1f3a66d2058c6f34f", null ],
    [ "sampleTime", "main_8py.html#afbc4f960652074bd7b93555fce0ee692", null ],
    [ "task1", "main_8py.html#af4b8f4290f8d32e70654f6deb864787f", null ],
    [ "task2", "main_8py.html#a99d189b8f9eb3784cf7d4e5c3241b562", null ],
    [ "task3", "main_8py.html#a120516a5d42c212c898d4616d266077a", null ],
    [ "Timer", "main_8py.html#a66e55ff0d6b45477f6098d2f6808ed81", null ]
];