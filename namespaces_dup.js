var namespaces_dup =
[
    [ "BLE", "namespace_b_l_e.html", null ],
    [ "BT_UI", "namespace_b_t___u_i.html", null ],
    [ "ClosedLoop", "namespace_closed_loop.html", null ],
    [ "DRV8847_MotorDriver", "namespace_d_r_v8847___motor_driver.html", null ],
    [ "Encoder", "namespace_encoder.html", null ],
    [ "FSM_Elevator", "namespace_f_s_m___elevator.html", null ],
    [ "ME305_lab1_V2", "namespace_m_e305__lab1___v2.html", null ],
    [ "ME305_Lab2", "namespace_m_e305___lab2.html", null ],
    [ "UI_detaGen", "namespace_u_i__deta_gen.html", null ],
    [ "UI_detaGenRefTracker", "namespace_u_i__deta_gen_ref_tracker.html", null ],
    [ "UI_detaGenStepResponse", "namespace_u_i__deta_gen_step_response.html", null ],
    [ "UI_frontend", "namespace_u_i__frontend.html", null ],
    [ "UI_frontendRefTracker", "namespace_u_i__frontend_ref_tracker.html", null ],
    [ "UI_frontendStepResponse", "namespace_u_i__frontend_step_response.html", null ]
];