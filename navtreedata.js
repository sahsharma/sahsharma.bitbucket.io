/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Sahil Sharma's ME 305 Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_introMain", null ],
    [ "Latest Lab Submission", "index.html#sec_lastLab", null ],
    [ "Latest Homework", "index.html#sec_lastHW", null ],
    [ "Lab 0x01 Fibonacci", "page__fib.html", [
      [ "Introduction", "page__fib.html#sec_introL1", null ],
      [ "fib Method", "page__fib.html#sec_fib", null ],
      [ "Results", "page__fib.html#sec_outputsL1", null ],
      [ "Relevant Files", "page__fib.html#sec_filesL1", null ],
      [ "Source Code Access", "page__fib.html#page_fib_src", null ]
    ] ],
    [ "Homework 0x00 Elevator", "page__elevator.html", [
      [ "Introduction", "page__elevator.html#sec_introH0", null ],
      [ "State Machine", "page__elevator.html#sec_stateMachineH0", null ],
      [ "Results", "page__elevator.html#sec_outputsH0", null ],
      [ "Relevant Files", "page__elevator.html#sec_filesH0", null ],
      [ "Source Code Access", "page__elevator.html#page_srcH0", null ]
    ] ],
    [ "Lab 0x02(b) Blinking LEDs", "page__l_e_ds.html", [
      [ "Introduction", "page__l_e_ds.html#sec_introL2", null ],
      [ "State Machine", "page__l_e_ds.html#sec_stateMachineL2", null ],
      [ "Results", "page__l_e_ds.html#sec_outputsL2", null ],
      [ "Relevant Files", "page__l_e_ds.html#sec_filesL2", null ],
      [ "Source Code Access", "page__l_e_ds.html#page_srcL2", null ]
    ] ],
    [ "Lab 0x03 Encoder with UI", "page__encoder_u_i.html", [
      [ "Introduction", "page__encoder_u_i.html#sec_introL3", null ],
      [ "State Machine", "page__encoder_u_i.html#sec_stateMachineL3", null ],
      [ "Results", "page__encoder_u_i.html#sec_outputsL3", null ],
      [ "Relevant Files", "page__encoder_u_i.html#sec_filesL3", null ],
      [ "Source Code Access", "page__encoder_u_i.html#page_srcL3", null ]
    ] ],
    [ "Lab 0x04 Encoder with More UI", "page__u_i_data_gen.html", [
      [ "Introduction", "page__u_i_data_gen.html#sec_introL4", null ],
      [ "State Machine", "page__u_i_data_gen.html#sec_stateMachineL4", null ],
      [ "Results", "page__u_i_data_gen.html#sec_outputsL4", null ],
      [ "Relevant Files", "page__u_i_data_gen.html#sec_filesL4", null ],
      [ "Source Code Access", "page__u_i_data_gen.html#page_srcL4", null ]
    ] ],
    [ "Lab 0x05 Bluetooth Interface and App", "page__b_l_e_driver.html", [
      [ "Introduction", "page__b_l_e_driver.html#sec_introL5", null ],
      [ "State Machine", "page__b_l_e_driver.html#sec_stateMachineL5", null ],
      [ "Results", "page__b_l_e_driver.html#sec_outputsL5", null ],
      [ "Relevant Files", "page__b_l_e_driver.html#sec_filesL5", null ],
      [ "Source Code Access", "page__b_l_e_driver.html#page_srcL5", null ]
    ] ],
    [ "Lab 0x06(a) Motor Driver", "page__motor_driver.html", [
      [ "Introduction", "page__motor_driver.html#sec_introL6", null ],
      [ "Results", "page__motor_driver.html#sec_outputsL6", null ],
      [ "Relevant Files", "page__motor_driver.html#sec_filesL6", null ],
      [ "Source Code Access", "page__motor_driver.html#sec_srcL6", null ]
    ] ],
    [ "Lab 0x06(b) Proportional Controller Motor Step Response", "page__step_response.html", [
      [ "Introduction", "page__step_response.html#sec_introL6b", null ],
      [ "State Machine", "page__step_response.html#sec_stateMachine6b", null ],
      [ "Results", "page__step_response.html#sec_outputsL6b", null ],
      [ "Relevant Files", "page__step_response.html#sec_filesL6b", null ],
      [ "Source Code Access", "page__step_response.html#sec_srcL6b", null ]
    ] ],
    [ "Lab 0x07 Reference Tracking", "page__ref_tracker.html", [
      [ "Introduction", "page__ref_tracker.html#sec_introL7", null ],
      [ "State Machine", "page__ref_tracker.html#sec_stateMachine7", null ],
      [ "Results", "page__ref_tracker.html#sec_outputsL7", null ],
      [ "Relevant Files", "page__ref_tracker.html#sec_filesL7", null ],
      [ "Source Code Access", "page__ref_tracker.html#sec_srcL7", null ]
    ] ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"",
"class_u_i__deta_gen_ref_tracker_1_1_ref_track_lab7.html#abad85454c293c8e0c4586d005f973889",
"main__lab7_8py.html#a5a8dbbe0e0d571bb29db94d6b60d2b16"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';