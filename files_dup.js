var files_dup =
[
    [ "BLE.py", "_b_l_e_8py.html", [
      [ "BluetoothDriver", "class_b_l_e_1_1_bluetooth_driver.html", "class_b_l_e_1_1_bluetooth_driver" ]
    ] ],
    [ "BT_UI.py", "_b_t___u_i_8py.html", [
      [ "BT_UI", "class_b_t___u_i_1_1_b_t___u_i.html", "class_b_t___u_i_1_1_b_t___u_i" ]
    ] ],
    [ "ClosedLoop.py", "_closed_loop_8py.html", [
      [ "CL", "class_closed_loop_1_1_c_l.html", "class_closed_loop_1_1_c_l" ]
    ] ],
    [ "DRV8847_MotorDriver.py", "_d_r_v8847___motor_driver_8py.html", [
      [ "Driver", "class_d_r_v8847___motor_driver_1_1_driver.html", "class_d_r_v8847___motor_driver_1_1_driver" ]
    ] ],
    [ "Encoder.py", "_encoder_8py.html", [
      [ "EncoderAB", "class_encoder_1_1_encoder_a_b.html", "class_encoder_1_1_encoder_a_b" ]
    ] ],
    [ "FSM_Elevator.py", "_f_s_m___elevator_8py.html", [
      [ "Elevator", "class_f_s_m___elevator_1_1_elevator.html", "class_f_s_m___elevator_1_1_elevator" ],
      [ "Button", "class_f_s_m___elevator_1_1_button.html", "class_f_s_m___elevator_1_1_button" ],
      [ "FloorSensor", "class_f_s_m___elevator_1_1_floor_sensor.html", "class_f_s_m___elevator_1_1_floor_sensor" ],
      [ "Motor", "class_f_s_m___elevator_1_1_motor.html", "class_f_s_m___elevator_1_1_motor" ]
    ] ],
    [ "main_hw0.py", "main__hw0_8py.html", "main__hw0_8py" ],
    [ "main_lab2.py", "main__lab2_8py.html", "main__lab2_8py" ],
    [ "main_lab3.py", "main__lab3_8py.html", "main__lab3_8py" ],
    [ "main_lab4.py", "main__lab4_8py.html", "main__lab4_8py" ],
    [ "main_lab4_PC.py", "main__lab4___p_c_8py.html", "main__lab4___p_c_8py" ],
    [ "main_lab5.py", "main__lab5_8py.html", "main__lab5_8py" ],
    [ "main_lab6.py", "main__lab6_8py.html", "main__lab6_8py" ],
    [ "main_lab6_PC.py", "main__lab6___p_c_8py.html", "main__lab6___p_c_8py" ],
    [ "main_lab7.py", "main__lab7_8py.html", "main__lab7_8py" ],
    [ "main_lab7_PC.py", "main__lab7___p_c_8py.html", "main__lab7___p_c_8py" ],
    [ "ME305_lab1_V2.py", "_m_e305__lab1___v2_8py.html", "_m_e305__lab1___v2_8py" ],
    [ "ME305_Lab2.py", "_m_e305___lab2_8py.html", [
      [ "VirtBlink", "class_m_e305___lab2_1_1_virt_blink.html", "class_m_e305___lab2_1_1_virt_blink" ],
      [ "LD2_LED", "class_m_e305___lab2_1_1_l_d2___l_e_d.html", "class_m_e305___lab2_1_1_l_d2___l_e_d" ],
      [ "VirtLED", "class_m_e305___lab2_1_1_virt_l_e_d.html", "class_m_e305___lab2_1_1_virt_l_e_d" ],
      [ "Duty", "class_m_e305___lab2_1_1_duty.html", "class_m_e305___lab2_1_1_duty" ]
    ] ],
    [ "UI.py", "_u_i_8py.html", [
      [ "UI_Encoder", "class_u_i_1_1_u_i___encoder.html", "class_u_i_1_1_u_i___encoder" ]
    ] ],
    [ "UI_detaGen.py", "_u_i__deta_gen_8py.html", [
      [ "EncoderData", "class_u_i__deta_gen_1_1_encoder_data.html", "class_u_i__deta_gen_1_1_encoder_data" ]
    ] ],
    [ "UI_detaGenRefTracker.py", "_u_i__deta_gen_ref_tracker_8py.html", [
      [ "RefTrackLab7", "class_u_i__deta_gen_ref_tracker_1_1_ref_track_lab7.html", "class_u_i__deta_gen_ref_tracker_1_1_ref_track_lab7" ]
    ] ],
    [ "UI_detaGenStepResponse.py", "_u_i__deta_gen_step_response_8py.html", [
      [ "EncoderDataLab6", "class_u_i__deta_gen_step_response_1_1_encoder_data_lab6.html", "class_u_i__deta_gen_step_response_1_1_encoder_data_lab6" ]
    ] ],
    [ "UI_frontend.py", "_u_i__frontend_8py.html", [
      [ "UI_Front", "class_u_i__frontend_1_1_u_i___front.html", "class_u_i__frontend_1_1_u_i___front" ]
    ] ],
    [ "UI_frontendRefTracker.py", "_u_i__frontend_ref_tracker_8py.html", [
      [ "UI_FrontLab7", "class_u_i__frontend_ref_tracker_1_1_u_i___front_lab7.html", "class_u_i__frontend_ref_tracker_1_1_u_i___front_lab7" ]
    ] ],
    [ "UI_frontendStepResponse.py", "_u_i__frontend_step_response_8py.html", [
      [ "UI_FrontLab6", "class_u_i__frontend_step_response_1_1_u_i___front_lab6.html", "class_u_i__frontend_step_response_1_1_u_i___front_lab6" ]
    ] ]
];