var searchData=
[
  ['datapoints_372',['datapoints',['../class_u_i__deta_gen_1_1_encoder_data.html#a6c503929918dc6efb8201596d320aad5',1,'UI_detaGen.EncoderData.datapoints()'],['../class_u_i__deta_gen_ref_tracker_1_1_ref_track_lab7.html#abf3510c8b9281c57c1a70fe07b4af558',1,'UI_detaGenRefTracker.RefTrackLab7.datapoints()'],['../class_u_i__deta_gen_step_response_1_1_encoder_data_lab6.html#a337618aaada11b78e3f824751d50dd7f',1,'UI_detaGenStepResponse.EncoderDataLab6.datapoints()'],['../main__lab6_8py.html#a24abd52a1ce3249005fa9dab5494fa6b',1,'main_lab6.datapoints()'],['../main__lab7_8py.html#a5a8dbbe0e0d571bb29db94d6b60d2b16',1,'main_lab7.datapoints()'],['../main__lab4_8py.html#a8872fdbe75becefa5f03434f755e6c29',1,'main_lab4.DataPoints()']]],
  ['dellevel_373',['dellevel',['../class_closed_loop_1_1_c_l.html#a02647eeb63b9d7794a27d4b0f8a13904',1,'ClosedLoop::CL']]],
  ['delta1_374',['delta1',['../class_encoder_1_1_encoder_a_b.html#a06e0952e05b9c95a9341a32f6c0dc9b9',1,'Encoder::EncoderAB']]],
  ['delta2_375',['delta2',['../class_encoder_1_1_encoder_a_b.html#a3c6201ae05e120c218f4042f1de8aa01',1,'Encoder::EncoderAB']]],
  ['duty_376',['duty',['../class_d_r_v8847___motor_driver_1_1_driver.html#a3f41715ef2893407f48d0786cc1564f9',1,'DRV8847_MotorDriver::Driver']]]
];
